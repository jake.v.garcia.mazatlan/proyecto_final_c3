import { initializeApp } from "https://www.gstatic.com/firebasejs/10.6.0/firebase-app.js";
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut } from "https://www.gstatic.com/firebasejs/10.6.0/firebase-auth.js";
import { getFirestore, doc, setDoc, getDoc, collection, query, addDoc, getDocs, updateDoc, deleteDoc } from "https://www.gstatic.com/firebasejs/10.6.0/firebase-firestore.js";

const firebaseConfig = {
    apiKey: "AIzaSyAWVjommpG2F1Gwg7hxdQx8XR2wcijvzgU",
    authDomain: "productos-28fe3.firebaseapp.com",
    projectId: "productos-28fe3",
    storageBucket: "productos-28fe3.appspot.com",
    messagingSenderId: "834188812321",
    appId: "1:834188812321:web:9442ca478dad64d5354360"
};
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const fireStore = getFirestore(app);

const loginContainer = document.querySelector("#loginContainer");
const productosContainer = document.querySelector("#productosContainer");
const registrarseOpcion = document.querySelector("#registrarseOpcion");
const salirOpcion = document.querySelector("#salirOpcion");
const idProductoInput = document.querySelector("#idProducto");
const nombreProducto = document.querySelector("#nombreProducto");
const precioProducto = document.querySelector("#precioProducto");

async function editarProducto(idProducto) {
    const productoRef = doc(fireStore, "productos", idProducto);
    try {
        const docSnapshot = await getDoc(productoRef);
        if (docSnapshot) {
            // Accede a los datos del documento
            const productoData = docSnapshot.data();
            idProductoInput.value = idProducto;
            nombreProducto.value = productoData.nombre;
            precioProducto.value = productoData.precio;
            $('#modalProductos').modal('show');
        } else {
            alert("El producto no existe");
        }
    } catch (error) {
        alert("Error al obtener el documento:", error);
    }
}

const eliminarProducto = async (idProducto) => {
    const productoRef = doc(fireStore, "productos", idProducto);

    try {
        await deleteDoc(productoRef);
        obtenerProductos();
    } catch (error) {
        alert("Error al eliminar el producto:", error);
    }
}

const obtenerProductos = async () => {
    const productosRef = collection(fireStore, "productos");
    const q = query(productosRef);
    const querySnapshot = await getDocs(q);
    let productos = [];
    querySnapshot.forEach((doc) => {
        productos.push({
            id: doc.id,
            ...doc.data()
        });
    });
    const buttonAdd = document.createElement("button");
    buttonAdd.classList.add("btn", "btn-primary", "mb-3");
    buttonAdd.style.float = 'right';
    buttonAdd.dataset.bsToggle = "modal";
    buttonAdd.dataset.bsTarget = "#modalProductos";
    buttonAdd.innerHTML = "Agregar";

    const tablaContainer = document.getElementById("productosContainer");
    tablaContainer.innerHTML = '';

    tablaContainer.appendChild(buttonAdd);

    // Crea la tabla
    const table = document.createElement("table");
    table.classList.add("table", "table-bordered");

    // Crea el encabezado
    const thead = document.createElement("thead");
    const headerRow = document.createElement("tr");
    const isAdmin = localStorage.getItem('rol') && localStorage.getItem('rol') === 'administrador';
    let cabeceras = ['Nombre', 'Precio'];

    if (isAdmin) {
        cabeceras.push('Acciones');
    }

    cabeceras.forEach(key => {
        const th = document.createElement("th");
        th.textContent = key;
        headerRow.appendChild(th);
    });
    thead.appendChild(headerRow);
    table.appendChild(thead);

    // Crea el cuerpo de la tabla
    const tbody = document.createElement("tbody");
    productos.forEach(item => {
        const row = document.createElement("tr");
        const td1 = document.createElement("td");
        td1.textContent = item.nombre;
        row.appendChild(td1);
        const td2 = document.createElement("td");
        td2.textContent = `$${item.precio}`;
        row.appendChild(td2);

        if (isAdmin) {
            const td3 = document.createElement("td");
            const editarIcono = document.createElement("i");
            editarIcono.classList.add("bi", "bi-pencil", "me-2", "icono-tabla");
            editarIcono.title = "Editar";
            editarIcono.addEventListener("click", () => editarProducto(item.id));
            td3.appendChild(editarIcono);

            // Crea el ícono de eliminar (aún no se ha implementado la función)
            const eliminarIcono = document.createElement("i");
            eliminarIcono.classList.add("bi", "bi-trash3", "icono-tabla");
            eliminarIcono.title = "Eliminar";
            eliminarIcono.addEventListener("click", () => eliminarProducto(item.id));
            td3.appendChild(eliminarIcono);
            row.appendChild(td3);
        }

        tbody.appendChild(row);
    });
    table.appendChild(tbody);

    // Agrega la tabla al contenedor
    tablaContainer.appendChild(table);
}

const validacionMostrar = () => {
    if (localStorage.getItem('rol')) {
        loginContainer.style.display = "none";
        productosContainer.style.display = "block";
        registrarseOpcion.style.display = "none";
        salirOpcion.style.display = "block";
        obtenerProductos();
    }
    else {
        loginContainer.style.display = "block";
        productosContainer.style.display = "none";
        registrarseOpcion.style.display = "block";
        salirOpcion.style.display = "none";
    }
}

validacionMostrar();

const registroForm = document.querySelector('#registroForm');
registroForm.addEventListener('submit', (e) => {
    e.preventDefault();

    const email = document.querySelector('#emailRegistro').value;
    const contra = document.querySelector('#contraRegistro').value;
    const rol = document.querySelector('#rolRegistro').value;

    if(!email || !contra || !rol) {
        alert('Ingrese todos los datos');
        return;
    }

    createUserWithEmailAndPassword(auth, email, contra).then(userCredentials => {
        const docRef = doc(fireStore, `usuarios/${userCredentials.user.uid}`);
        setDoc(docRef, { email: email, rol: rol });
        $('#modalRegistro').modal('hide');
    }).catch(() => {
        alert('Error al intentarse registrar');
    });;
});

const loginForm = document.querySelector('#loginForm');
loginForm.addEventListener('submit', (e) => {
    e.preventDefault();

    const email = document.querySelector('#emailLogin').value;
    const contra = document.querySelector('#contraLogin').value;

    signInWithEmailAndPassword(auth, email, contra).then(async userCredentials => {
        const docRef = doc(fireStore, `usuarios/${userCredentials.user.uid}`);
        const encryptedData = await getDoc(docRef);
        const data = encryptedData.data().rol;
        localStorage.setItem('rol', data);
        validacionMostrar();
    }).catch(() => {
        alert('Error al intentarse loguear');
    });
});

const productosForm = document.querySelector('#productosForm');
productosForm.addEventListener('submit', async (e) => {
    e.preventDefault();

    if(!nombreProducto.value || !precioProducto.value) {
        alert('Ingrese todos los datos');
        return;
    }

    const productoDatos = {
        nombre: nombreProducto.value,
        precio: precioProducto.value,
    };
    if (idProductoInput.value) {
        const productoRef = doc(fireStore, 'productos', idProductoInput.value);

        try {
            await updateDoc(productoRef, productoDatos);
        } catch (error) {
            alert('Error al actualizar el producto:', error);
        }
    }
    else {
        const productosRef = collection(fireStore, 'productos');

        try {
            await addDoc(productosRef, productoDatos);

        } catch (error) {
            alert('Error al agregar el producto:', error);
        }
    }
    obtenerProductos();
    $('#modalProductos').modal('hide');
});

const modalProductos = document.getElementById('modalProductos');

// Agrega un evento que se ejecutará cuando se cierre el modal
modalProductos.addEventListener('hidden.bs.modal', function () {
    idProductoInput.value = '';
    nombreProducto.value = '';
    precioProducto.value = '';
});

salirOpcion.addEventListener('click', (e) => {
    e.preventDefault();
    signOut(auth).then(() => {
        localStorage.removeItem('rol');
        validacionMostrar();
    });
});